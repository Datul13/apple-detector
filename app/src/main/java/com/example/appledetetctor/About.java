package com.example.appledetetctor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class About extends AppCompatActivity {

    Toolbar tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        BottomNavigationView navigasi = findViewById(R.id.menu_bawah);
        navigasi.setSelectedItemId(R.id.about);

        navigasi.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        startActivity(new Intent(getApplicationContext()
                                , Model.class));
                        overridePendingTransition(0, 0);
                        return true;

                    case R.id.about:
                        return true;

                    case R.id.help:
                        startActivity(new Intent(getApplicationContext()
                                , Help.class));
                        overridePendingTransition(0, 0);
                        return true;

                }

                return false;

            }
        } );

        tb = findViewById(R.id.tb);
        tb.setTitle("About");
        setSupportActionBar(tb);

    }
}